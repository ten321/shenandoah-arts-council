<?php
/**
 * Shenandoah Arts Council Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package shenarts
 */

namespace {
	if ( ! defined( 'TRIBE_HIDE_MARKETING_NOTICES' ) ) {
		define( 'TRIBE_HIDE_MARKETING_NOTICES', true );
	}

	spl_autoload_register( function ( $class_name ) {
		if ( ! stristr( $class_name, 'ShenArts\\' ) ) {
			return;
		}

		$filename = get_stylesheet_directory() . '/lib/classes/' . strtolower( str_replace( array(
				'\\',
				'_'
			), array( '/', '-' ), $class_name ) ) . '.php';

		if ( ! file_exists( $filename ) ) {
			return;
		}

		include_once $filename;
	} );
}

namespace ShenArts {
	theme::instance();
}