<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$template = wc_get_theme_slug_for_templates();

genesis_markup( array(
	'open'   => '<div %s>',
	'context' => 'content-sidebar-wrap',
) );

do_action( 'genesis_before_content' );
genesis_markup( array(
	'open'   => '<main %s>',
	'context' => 'content',
) );
do_action( 'genesis_before_loop' );