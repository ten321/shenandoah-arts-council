jQuery( function() {
    if ( ! ( 'selector' in SASponsorsObj ) ) {
        /*console.log( 'The selector property was not found inside of SASponsorsObj' );
        console.log( SASponsorsObj );*/
        return;
    }
    if ( document.querySelectorAll( SASponsorsObj.selector ).length <= 0 ) {
        /*console.log( 'No matching elements were found in the page' );*/
        return;
    }

    jQuery( SASponsorsObj.selector ).each( function() {
        var url = SASponsorsObj.url;
        var instance_id = jQuery( this ).attr( 'id' );
        var args = SASponsorsObj.instance[instance_id].args;
        var urlArgs = {
            'orderby' : args.orderby,
            'order' : args.order,
            'per_page' : args.number,
            '_embed' : 1
        };

        jQuery.get( url, urlArgs, function( data ) {
            /*console.log( data );*/
            var obj = jQuery( '#' + instance_id );
            obj.empty();
            for ( var p in data ) {
                console.log( data[p] );
                var feature = data[p]._embedded['wp:featuredmedia'][0];

                if ( data[p].link.indexOf( '#new_tab' ) > 0 ) {
                    data[p].link = data[p].link.replace( /#new_tab/, '' )
                    data[p].target = ' target="_blank"';
                } else {
                    data[p].target = '';
                }

                var featureURL = feature.source_url;

                if ( 'full' in feature.media_details.sizes ) {
                    featureURL = feature.media_details.sizes.full.source_url;
                }

                var code = '<li><a href="' + data[p].link + '"' + data[p].target + '><img src="' + featureURL + '" alt="' + feature.alt_text + '"/></a></li>';
                jQuery( code ).appendTo( obj );
            }
        }, 'json' );
    } );
} );