jQuery( function() {
    function makeSpaceForHeader() {
        var h = jQuery( '.site-header' );
        var pad = h.outerHeight();
        var pos = 'fixed';
        var off = 0;
        if ( jQuery( '.site-header .mega-menu-toggle' ).is( ':visible' ) ) {
            pad = 0;
            pos = 'static';
        }
        if ( jQuery( 'body' ).hasClass( 'admin-bar' ) ) {
            off = jQuery( '#wpadminbar' ).outerHeight();
        }
        console.log( 'Preparing to set padding-top for the site-inner element to ' + pad + 'px' );
        jQuery( '.site-inner' ).css( { 'padding-top' : pad + 'px' } );
        h.css( { 'position' : pos, 'top' : off + 'px' } );
    }

    jQuery( window ).on( 'resize load', function() { makeSpaceForHeader(); } );
} );