<?php
/**
 * Primary class defintions for the Shenandoah Arts 2018 WordPress theme
 */
namespace ShenArts;

class theme {
	/**
	 * Holds the version number for use with various assets
	 *
	 * @since  0.1
	 * @access public
	 * @var    string
	 */
	public $version = '1.1.7';

	/**
	 * Determines whether scripts should be minified or not
     *
     * @since  2.0
     * @access public
     * @var    string
	 */
	public $script_extension = 'js';

	/**
	 * Holds the class instance.
	 *
	 * @since     0.1
	 * @access    private
	 * @var        \ShenArts\theme
	 */
	private static $instance;

	/**
	 * Returns the instance of this class.
	 *
	 * @access  public
	 * @since   0.1
	 * @return    \ShenArts\theme
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className      = __CLASS__;
			self::$instance = new $className;
		}

		return self::$instance;
	}

	/**
	 * Create the object and set up the appropriate actions
	 *
	 * @access  private
	 * @since   0.1
	 */
	private function __construct() {
		//* Child theme (do not remove)
		define( 'CHILD_THEME_NAME', 'Shenandoah Arts Council' );
		define( 'CHILD_THEME_VERSION', $this->version );

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
		    $this->script_extension = 'js';
        } else {
		    $this->script_extension = 'min.js';
        }

		$this->_add_hooks();
	}

	/**
	 * Add any necessary hooks for this theme
     *
     * @access private
     * @since  2.0
     * @return void
	 */
	private function _add_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
		add_action( 'template_redirect', array( $this, 'template_redirect' ) );

		add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
		add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
		add_action( 'init', array( $this, 'init' ) );

		add_action( 'widgets_init', function() { register_widget( 'ShenArts\Widgets\Sponsors' ); } );

		add_action( 'dynamic_sidebar', array( $this, 'add_excerpt_read_more' ) );
		add_action( 'dynamic_sidebar_after', array( $this, 'remove_excerpt_read_more' ) );

		add_filter( 'user_contactmethods', array( $this, 'remove_extra_contact_fields' ) );

		add_action( 'gform_user_registered', array( $this, 'complete_vendor_setup' ), 10, 4 );

		add_action( 'updated_user_meta', array( $this, 'sync_profile_photo_with_logo' ), 10, 4 );

		add_action( 'rest_api_init', array( $this, 'rest_api_init' ) );

		/* Remove Page Links To from WooCommerce post types */
        add_filter( 'page-links-to-post-types', array( $this, 'remove_plt_from_post_types' ) );

		add_action( 'admin_menu', array( $this, 'cleanup_admin_menu' ), 99 );
		add_action( 'load-profile.php', function() {
			if ( ! current_user_can( 'delete_plugins' ) ) {
				wp_safe_redirect( home_url( '/user/?profiletab=main&um_action=edit' ) );
			}
		} );

		if ( is_admin() ) {
		    add_action( 'admin_head', array( $this, 'remove_media_buttons' ) );
		    add_action( 'admin_print_styles', array( $this, 'do_admin_styles' ) );
        }
    }

	/**
	 * Output any special styles for the admin area
     *
     * @access public
     * @since  2.0
     * @return void
	 */
    public function do_admin_styles() {
	    echo '<style type="text/css">#woocommerce-product-data .hndle span span.hidden, #woocommerce-product-data ._wcpv_product_commission_field {display: none;}#woocommerce-product-data ul.wc-tabs li.linked_product_options, #woocommerce-product-data ul.wc-tabs li.attribute_options {display: none;}</style>';
    }

	/**
	 * Removes the media buttons from above the post editor when the user
     *      shouldn't have access to those things
     *
     * @access public
     * @since  2.0
     * @return void
	 */
	public function remove_media_buttons() {
	    global $post;
	    if ( ! is_object( $post ) )
	        return;

	    if ( ! property_exists( $post, 'post_type' ) )
	        return;

	    if ( 'product' !== $post->post_type )
	        return;

	    if ( current_user_can( 'delete_users' ) )
	        return;

	    if ( is_object( $post ) && property_exists( $post, 'post_type' ) && 'product' == $post->post_type ) {
	        remove_all_actions( 'media_buttons' );
        }
    }

	/**
	 * Remove the Page Links To metabox from certain post types
     * @param array $post_types the list of existing post types
     *
     * @access public
     * @since  1.0
     * @return array the updated list of post types
	 */
	public function remove_plt_from_post_types( $post_types ) {
        $blacklist = $this->get_woo_post_types();

        $whitelist = array();

        foreach ( $post_types as $t ) {
            if ( ! in_array( $t, $blacklist ) ) {
                $whitelist[] = $t;
            }
        }

        return $whitelist;
    }

	/**
	 * Retrieve a list of WooCommerce post types
     *
     * @access private
     * @since  1.0
     * @return array the list of WC post types
	 */
	private function get_woo_post_types() {
	    return array( 'product', 'product_variation', 'product_visibility', 'shop_order', 'shop_coupon', 'shop_webhook' );
    }

	/**
     * Add a filter to excerpts to force a read more link
	 * @param $widget
     *
     * @access public
     * @since  0.1
     * @return void
	 */
	public function add_excerpt_read_more( $widget ) {
	    if ( ! stristr( $widget['id'], 'genesisresponsiveslider' ) ) {
	        return;
        }

        add_filter( 'the_excerpt', array( $this, 'do_excerpt_read_more' ) );
    }

	/**
	 * Remove the filter that forces a read more link on excerpts
     *
     * @access public
     * @since  0.1
     * @return void
	 */
    public function remove_excerpt_read_more() {
	    if ( ! has_filter( 'the_excerpt', array( $this, 'add_excerpt_read_more' ) ) ) {
	        return;
        }

        remove_filter( 'the_excerpt', array( $this, 'add_excerpt_read_more' ) );
    }

	/**
     * Add a read more link to a post excerpt
	 * @param $excerpt
	 *
     * @access public
     * @since  0.1
	 * @return string
	 */
    public function do_excerpt_read_more( $excerpt ) {
	    return $excerpt . sprintf( '<p><a href="%s" title="%s">%s</a></p>', get_permalink(), sprintf( __( 'Read the rest of %s' ), get_the_title() ), __( 'Read more' ) );
    }

	/**
	 * Enqueue scripts and styles
	 */
	public function add_scripts_and_styles() {
		wp_enqueue_script( 'shenarts-scripts', get_stylesheet_directory_uri() . '/lib/scripts/shenarts/theme/scripts.' . $this->script_extension, array( 'jquery' ), $this->version, true );
		wp_register_style( 'genesis-style', get_stylesheet_directory_uri() . '/style.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'shenarts-style',
			get_stylesheet_directory_uri() . '/lib/styles/shenarts/theme/style.css',
			array( 'genesis-style' ),
			$this->version,
			'all'
		);
	}

	/**
	 * Perform any actions that need to happen once the template is loaded
     *
     * @access public
     * @since  0.1
     * @return void
	 */
	public function template_redirect() {
		if ( is_front_page() ) {
			remove_all_actions( 'genesis_before_entry' );
			remove_all_actions( 'genesis_entry_header' );
			remove_all_actions( 'genesis_entry_content' );
			remove_all_actions( 'genesis_after_entry' );
			add_action( 'genesis_entry_content', array( $this, 'do_home_content' ) );
		} else if ( is_page() || is_single() ) {
			add_action( 'genesis_before_content', array( $this, 'insert_page_hero' ) );
		} else if ( is_tax( 'wcpv_product_vendors' ) || is_woocommerce() ) {
		    if ( is_tax( 'wcpv_product_vendors' ) ) {
		        $this->redirect_vendor_to_um_profile();
		    }
			add_filter( 'genesis_site_layout', '__genesis_return_sidebar_content' );
			remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
			add_action( 'genesis_sidebar', array( $this, 'do_vendor_sidebar' ) );
			remove_action( 'genesis_loop', 'genesis_do_loop' );
			add_action( 'genesis_loop', array( $this, 'do_vendor_loop' ) );
		}
	}

	/**
	 * Attempt to identify Vendor Admin user Member profile page & redirect
     *
     * @access private
     * @since  2.0
     * @return void
	 */
	private function redirect_vendor_to_um_profile() {
		$ob = get_queried_object();
		$vendor_data = get_term_meta( $ob->term_id, 'vendor_data' );
		$vendor_data = @array_shift( $vendor_data );

		if ( ! is_array( $vendor_data ) ) {
			return;
		}

		/*var_dump( $vendor_data );*/

		$vendor = get_user_by( 'email', $vendor_data['email'] );
		/*var_dump( $vendor );*/
		if ( ! is_a( $vendor, 'WP_User' ) ) {
			$vendor = get_user_by( 'id', $vendor_data['admins'][0] );
		}
		if ( ! is_a( $vendor, 'WP_User' ) ) {
			return;
		}

		if ( function_exists( 'um_user_profile_url' ) ) {
			header( "Location: " . um_user_profile_url( $vendor->id ) );
			die();
		}

		return;
	}

	/**
	 * Perform any necessary modifications to the Genesis defaults
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function adjust_genesis() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
		add_theme_support( 'genesis-responsive-viewport' );
		add_theme_support( 'genesis-accessibility', array(
			'404-page',
			'drop-down-menu',
			'headings',
			'rems',
			'search-form',
			'skip-links'
		) );
		add_theme_support( 'genesis-structural-wraps', array(
			'header',
			'menu-primary',
			'menu-secondary',
			'site-inner',
			'footer-widgets',
			'footer',
			'front-sidebar',
		) );

		/** Add support for custom header **/
		$header_args = array(
			'default-image'          => get_stylesheet_directory_uri() . '/images/header-logo.png',
			'width'                  => 212,
			'height'                 => 158,
			'flex-width'             => false,
			'flex-height'            => true,
			'uploads'                => true,
			'random-default'         => false,
			'header-text'            => false,
			'default-text-color'     => '',
			'wp-head-callback'       => array( $this, 'do_header_style' ),
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
			'video'                  => false,
			'video-active-callback'  => '',
		);
		/*add_theme_support( 'custom-header', $header_args );*/

		/** Add support for a custom logo */
		add_theme_support( 'custom-logo', array(
			'default-image' => get_stylesheet_directory_uri() . '/images/header-logo.png',
			'width'         => 212,
			'height'        => 158,
			'flex-height'   => true,
		) );

		remove_action( 'genesis_header', 'genesis_do_header' );
		add_action( 'genesis_header', array( $this, 'do_header' ) );
		remove_action( 'genesis_footer', 'genesis_do_footer' );
		add_action( 'genesis_footer', array( $this, 'do_footer' ) );

		remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
		global $content_width;
		$content_width = 1280;

		$this->adjust_um_display();
	}

	/**
	 * Adjust the way Ultimate Member profiles are displayed
     *
     * @access private
     * @since  2.0
     * @return void
	 */
	private function adjust_um_display() {
		add_filter('um_profile_tabs', array( $this, 'add_um_gallery_tab' ), 1000 );
		add_action( 'um_profile_content_vendorgallery_default', array( $this, 'do_um_gallery_tab' ) );

		remove_action( 'um_after_profile_header_name_args', 'um_social_links_icons', 50 );
		add_action( 'um_after_profile_header_name_args', array( $this, 'um_show_social_urls' ), 50 );
    }

	/**
	 * Add a "Gallery" tab to the Ultimate Member profile
     * @param array $tabs the array of existing tabs
     *
     * @access public
     * @since  2.0
     * @return array the updated array of tabs
	 */
	public function add_um_gallery_tab( $tabs=array() ) {
	    $tabs['vendorgallery'] = array(
		    'name' => __( 'Art Gallery', 'shenarts' ),
		    'icon' => 'um-faicon-paint-brush',
	    );

	    return $tabs;
    }

	/**
	 * Output the content of the WC Vendor Gallery on the UM Profile page
     *
     * @access public
     * @since  2.0
     * @return void
	 */
	public function do_um_gallery_tab() {
	    $user_id = \UM()->user()->id;
        $term = \WC_Product_Vendors_Utils::get_all_vendor_data( $user_id );
        if ( empty ( $term ) || is_wp_error( $term ) || ! is_array( $term ) ) {
            _e( 'This user does not appear to have a gallery, yet.', 'shenarts' );
            return;
        }

        $term = array_shift( $term );
		$vendor_data = \WC_Product_Vendors_Utils::get_vendor_data_by_id( $term['term_id'] );

        $args = array(
            'post_type' => 'product',
            'tax_query' => array( array(
                'taxonomy' => \WC_PRODUCT_VENDORS_TAXONOMY,
                'field' => 'term_id',
                'terms' => $term['term_id'],
            ) ),
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        $loop = new \WP_Query( $args );
		$this->do_vendor_product_loop( $loop );
		wp_reset_query();
		wp_reset_postdata();
		return;
    }

	/**
	 * Output the vendor product list within the UM member profile tab
     * @param \WP_Query $loop the query for the product loop
     *
     * @access public
     * @since  2.0
     * @return void
	 */
    public function do_vendor_product_loop( $loop ) {
        global $wp_query;
        $wp_query = $loop;

        echo '<div class="woocommerce">';

        add_filter( 'option_wcpv_vendor_settings_vendor_display_logo', '__return_false' );

	    if ( $loop->have_posts() ) {
	        do_action( 'woocommerce_archive_description' );
		    /**
		     * Hook: woocommerce_before_shop_loop.
		     *
		     * @hooked wc_print_notices - 10
		     * @hooked woocommerce_result_count - 20
		     * @hooked woocommerce_catalog_ordering - 30
		     */
		    do_action( 'woocommerce_before_shop_loop' );
		    woocommerce_product_loop_start();
            while ( $loop->have_posts() ) {
                $loop->the_post();
                /**
                 * Hook: woocommerce_shop_loop.
                 *
                 * @hooked WC_Structured_Data::generate_product_data() - 10
                 */
                do_action( 'woocommerce_shop_loop' );
                wc_get_template_part( 'content', 'product' );
            }
		    woocommerce_product_loop_end();
		    /**
		     * Hook: woocommerce_after_shop_loop.
		     *
		     * @hooked woocommerce_pagination - 10
		     */
		    do_action( 'woocommerce_after_shop_loop' );
	    } else {
		    /**
		     * Hook: woocommerce_no_products_found.
		     *
		     * @hooked wc_no_products_found - 10
		     */
		    do_action( 'woocommerce_no_products_found' );
	    }

	    echo '</div>';

	    remove_filter( 'option_wcpv_vendor_settings_vendor_display_logo', '__return_false' );

	    return;
    }

	public function register_sidebars() {
		/**
		 * Register the front page widget areas
		 */
		genesis_register_sidebar( array(
			'id'          => 'front-feature',
			'name'        => __( '[Front Page] Main Feature', 'shenarts' ),
			'description' => __( 'Appears full-width at the top of the home page', 'shenarts' ),
		) );

		genesis_register_sidebar( array(
			'id'          => 'front-secondary',
			'name'        => __( '[Front Page] Secondary Features', 'shenarts' ),
			'description' => __( 'Appears below the main feature; items are divided into thirds across the full-width page', 'shenarts' ),
		) );

		/**
		 * Register the footer widget area
		 */
		genesis_register_sidebar( array(
			'id'          => 'shenarts-footer',
			'name'        => __( 'Footer Widgets', 'shenarts' ),
			'description' => __( 'Appears in the footer of the website throughout', 'shenarts' ),
		) );

		/**
		 * Register the WooCommerce sidebar
		 */
		genesis_register_sidebar( array(
			'id'          => 'shop',
			'name'        => __( 'Shop', 'shenarts' ),
			'description' => __( 'Appears on all shop pages', 'shenarts' ),
		) );
	}

	/**
	 * Perform any actions that need to happen on the init hook
     *
     * @access public
     * @since  0.1
     * @return void
	 */
	public function init() {
        $types = Types::instance();

		add_filter( 'widget_title', 'do_shortcode' );
		add_image_size( 'page-hero', 1200, 380, true );
		add_image_size( 'home-slide', 900, 380, true );
		add_image_size( 'home-bottom', 640, 180, true );
		/*add_filter( 'bfa_icon', function( $output ) { if ( stristr( $output, 'fas ' ) ) { $output = str_ireplace( 'fas ', 'far ', $output ); } return $output; }, 99 );*/
	}

	/**
	 * Set up any custom REST API handlers
     *
     * @access public
     * @since  2.0
     * @return void
	 */
	public function rest_api_init() {
		add_filter( 'rest_sponsor_collection_params', array( $this, 'rest_sponsor_rand' ), 10, 1 );
    }

	/**
	 * Add rand as an orderby parameter for sponsor REST API queries
     * @param array $params the array of parameters
     *
     * @access public
     * @since  2.0
     * @return array the updated array of parameters
	 */
	public function rest_sponsor_rand( $params ) {
	    $params['orderby']['enum'][] = 'rand';
	    return $params;
	}

	/*
	 * Output the page header
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function do_header() {
		global $wp_registered_sidebars;

		if ( function_exists( 'the_custom_logo' ) ) {
			the_custom_logo();
		}
		if ( has_action( 'genesis_header_right' ) || ( isset( $wp_registered_sidebars['header-right'] ) && is_active_sidebar( 'header-right' ) ) ) {

			genesis_markup( array(
				'open'    => '<div %s>',
				'context' => 'header-widget-area',
			) );

			/**
			 * Fires inside the header widget area wrapping markup, before the Header Right widget area.
			 *
			 * @since 1.5.0
			 */
			do_action( 'genesis_header_right' );
			add_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			add_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );
			dynamic_sidebar( 'header-right' );
			remove_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			remove_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );

			genesis_markup( array(
				'close'   => '</div>',
				'context' => 'header-widget-area',
			) );

		}
	}

	/**
	 * Output the custom footer for this site
	 *
	 * @access public
	 * @since 0.1
	 * @return void
	 */
	public function do_footer() {
		dynamic_sidebar( 'shenarts-footer' );
		echo '<p class="site-attribution">Site by White Spider, Inc.</p>';
	}

	/**
	 * Output the CSS declaration for the custom header image
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function do_header_style() {
		printf( '<style type="text/css">.site-header > .wrap { background-image: url(%s) }</style>', get_header_image() );
	}

	/**
	 * Output the widget areas for the home page
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function do_home_content() {
		dynamic_sidebar( 'front-feature' );
		if ( is_active_sidebar( 'front-secondary' ) ) {
			echo '<div class="front-secondary"><div class="wrap">';
			dynamic_sidebar( 'front-secondary' );
			echo '</div></div>';
		}
	}

	/**
	 * Insert the page hero/featured image at the top of the content area
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function insert_page_hero() {
		if ( ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular( 'sponsor' ) ) {
		    add_filter( 'the_content', array( $this, 'insert_sponsor_feature' ) );
		    return;
        }

		$thumb = get_the_post_thumbnail( get_the_ID(), 'page-hero' );
		if ( ! is_wp_error( $thumb ) && ! empty( $thumb ) ) {
			printf( '<header class="page-hero">%s</header>', $thumb );
		}

		return;
	}

	/**
	 * Insert the sponsor logo/featured image as part of the content on a singular sponsor post
     * @param  string $content the content being filtered
     * @see    apply_filters( 'the_content' )
     *
     * @access public
     * @since  2.0
     * @return string the updated content
	 */
	public function insert_sponsor_feature( $content ) {
	    if ( ! is_main_query() ) {
	        return $content;
        }

        if ( ! is_singular( 'sponsor' ) ) {
	        return $content;
        }

        if ( ! has_post_thumbnail() ) {
	        return $content;
        }

        $image = get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'alignright' ) );
	    return $image . $content;
    }

	/**
	 * Output a loop of products associated with a specific vendor
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function do_vendor_loop() {
		wc_get_template_part( 'archive', 'product' );
	}

	/**
	 * Output a special sidebar specifically for vendor pages
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function do_vendor_sidebar() {
	    if ( is_active_sidebar( 'shop' ) ) {
	        dynamic_sidebar( 'shop' );
	        return;
        }
		?>
		<header class="woocommerce-products-header">
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
			<?php endif; ?>

			<?php
			/**
			 * Hook: woocommerce_archive_description.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
			?>
		</header>
		<?php
		/**
		 * Hook: woocommerce_sidebar.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	}

	/**
	 * Remove the duplicate profile fields for social media
     * @param array $fields the array of existing contactmethods fields
     *
     * @access public
     * @since  2.0
     * @return array
	 */
	public function remove_extra_contact_fields( $fields=array() ) {
	    $remove = array( 'url', 'googleplus', 'twitter', 'facebook' );
	    foreach( $remove as $r ) {
	        if ( array_key_exists( $r, $fields ) ) {
	            unset( $fields[$r] );
            }
        }

        return $fields;
    }

	/**
     * Output the social icons in the Ultimate Member grid or individual profile
     *
     * @access public
     * @since  2.0
     * @return void
     */
	public function um_show_social_urls() {
	    $fields = get_fields( sprintf( 'user_%d', \UM()->user()->id ) );
	    if ( ! is_array( $fields ) ) {
	        return;
        }
	    $icons = array();

        foreach ( $fields as $name=>$value ) {
            if ( empty( $value ) ) {
                continue;
            }

            $label = '';

            switch ( $name ) {
                case 'instagram_username' :
                    $value = esc_url( sprintf( 'https://www.instagram.com/%s/', $value ) );
                    $class = 'instagram';
                    break;
                case 'twitter_username' :
                    if ( substr( $value, 0, 1 ) == '@' ) {
                        $value = substr( $value, 1 );
                    }
                    $value = esc_url( sprintf( 'https://twitter.com/%s/', $value ) );
                    $class = 'twitter';
                    break;
                case 'youtube_channel' :
                    $class = 'youtube';
                    $value = esc_url( $value );
                    break;
                case 'website_url' :
                    $class = 'globe';
                    $value = esc_url( $value );
                    $label = 'Website';
                    break;
                default :
                    $class = str_replace( '_profile', '', $name );
                    $value = esc_url( $value );
                    break;
            }

            if ( empty( $label ) ) {
                $label = ucfirst( $class );
            }
            $icons[$class] = sprintf( '<a class="umw-tip-n %2$s" href="%1$s" title="%3$s" style="background:#333"><i class="um-faicon-%2$s"></i></a>', $value, $class, $label );
        }

        if ( ! empty( $icons ) ) {
            printf( '<div class="um-profile-connect um-member-connect">%s</div>', implode( '', $icons ) );
        }

		return;

        echo '<style type="text/css">.um-tip-n.facebook{background:#3B5999} .um-tip-n.twitter{background:#4099FF} .um-tip-n.linkedin{background:#0072b1} .um-tip-n.youtube{background:#df2822} .um-tip-n.pinterest{background:#c91e26}</style>';
    }

    /**
     * Upon submission of the registration form, we need to set up a new Vendor,
     *      and perform some other basic tasks to set up the user profile
     * @param $user_id int the ID of the user that was created
     * @param $feed array the GravityForms user feed being processed
     * @param $entry array the GravityForms form entry being processed
     * @param $user_pass string the password set for the user
     *
     * @access public
     * @since  2.0
     * @return void
     */
    public function complete_vendor_setup( $user_id=0, $feed=array(), $entry=array(), $user_pass='' ) {
        if ( ! in_array( intval( $feed['id'] ), array( 2, 6 ) ) || ! in_array( intval( $feed['form_id'] ), array( 3, 7 ) ) ) {
	        return;
        }

        $defaults = array(
            'firstname' => '',
            'lastname' => '',
            'username' => '',
            'email' => '',
            'confirm_email' => '',
            'vendor_name' => '',
            'vendor_description' => '',
        );

        $args = array(
            'firstname' => rgar( $entry, '1.3' ),
            'lastname' => rgar( $entry, '1.6' ),
            'username' => rgar( $entry, '5' ),
            'email' => rgar( $entry, '5.1' ),
            'confirm_email' => rgar( $entry, '5.2' ),
            'vendor_name' => rgar( $entry, '31' ),
            'vendor_description' => rgar( $entry, '26' ),
        );

        if ( empty( $args['vendor_name'] ) ) {
            $args['vendor_name'] = $args['firstname'] . ' ' . $args['lastname'];
        }

        $user = get_user_by( 'id', $user_id );

        $this->create_vendor( $args, $user );

        $this->role_update( $user_id, 'wc_product_vendors_admin_vendor', array( 'wc_product_vendors_pending_vendor' ) );

        return;
    }

	/**
     * Create a new "Vendor" when a new user registers
	 * @param $form_items array the items that were sent through the user registration form
	 * @param $user \WP_User|\WP_Error the user that was created through the registration form
	 * @param array $args any additional arguments
	 *
	 * @return bool
	 */
    public function create_vendor( $form_items, $user, $args = array() ) {
	    $term_args = apply_filters( 'wcpv_registration_term_args', $args, $form_items );

	    // add vendor name to taxonomy
	    $term = wp_insert_term( $form_items['vendor_name'], WC_PRODUCT_VENDORS_TAXONOMY, $term_args );

	    // no errors, term added, continue
	    if ( ! is_wp_error( $term ) && ! empty( $user ) ) {
		    // add user to term meta
		    $vendor_data = array();

		    $vendor_data['admins']               = $user->ID;
		    $vendor_data['per_product_shipping'] = 'yes';
		    $vendor_data['commission_type']      = 'percentage';

		    update_term_meta( $term['term_id'], 'vendor_data', apply_filters( 'wcpv_registration_default_vendor_data', $vendor_data ) );

		    // change this user's role to pending vendor
		    wp_update_user( apply_filters( 'wcpv_registration_default_user_data', array(
			    'ID'   => $user->ID,
			    'role' => 'wc_product_vendors_admin_vendor',
		    ) ) );

		    $default_args = array(
			    'user_id'     => $user->ID,
			    'user_email'  => $user->user_email,
			    'first_name'  => $user->user_firstname,
			    'last_name'   => $user->user_lastname,
			    'user_login'  => __( 'Same as your account login', 'woocommerce-product-vendors' ),
			    'user_pass'   => __( 'Same as your account password', 'woocommerce-product-vendors' ),
			    'vendor_name' => $form_items['vendor_name'],
			    'vendor_desc' => $form_items['vendor_description'],
		    );

		    $args = apply_filters( 'wcpv_registration_args', wp_parse_args( $args, $default_args ), $args, $default_args );

		    do_action( 'wcpv_shortcode_registration_form_process', $args, $form_items );
	    } else {
		    global $errors;

		    if ( is_wp_error( $user ) ) {
			    $errors[] = $user->get_error_message();
		    }

		    if ( is_wp_error( $term ) ) {
			    $errors[] = $term->get_error_message();
		    }
	    }

	    return true;
    }

	/**
     * Make sure the user is set as a Vendor Admin & trigger the email notifying them of that
	 * @param $user_id int the ID of the user being updated
	 * @param $new_role string the slug of the new role
	 * @param array $old_roles the list of old roles to be removed
	 *
	 * @return bool
	 */
    public function role_update( $user_id, $new_role, $old_roles=array() ) {
	    $vendor_roles = array( 'wc_product_vendors_admin_vendor', 'wc_product_vendors_manager_vendor' );

	    $approved_already = get_user_meta( $user_id, '_wcpv_vendor_approval', true );

	    // Remove vendor approval if vendor role is changed to non vendor role.
	    foreach ( $old_roles as $old_role ) {
		    if ( in_array( $old_role, $vendor_roles ) && ! in_array( $new_role, $vendor_roles ) ) {
			    delete_user_meta( $user_id, '_wcpv_vendor_approval' );
		    }
	    }

	    if (
		    ! in_array( $new_role, $old_roles ) &&
		    in_array( $new_role, $vendor_roles ) &&
		    'yes' !== $approved_already
	    ) {
		    $emails = \WC()->mailer()->get_emails();

		    if ( ! empty( $emails ) ) {
			    $emails['WC_Product_Vendors_Approval']->trigger( $user_id, $new_role, $old_roles );
		    }
	    }

	    // Remove pending new vendor from saved list.
	    \WC_Product_Vendors_Utils::delete_new_pending_vendor( $user_id );

	    return true;
    }

    /**
     * Ensure that the Vendor Logo is the same as the UM Profile Photo
     *
     * @access public
     * @since  2.0
     * @return void
     */
    public function sync_profile_photo_with_logo( $meta_id, $object_id, $meta_key, $_meta_value ) {
        return;
    }

    /**
     * Attempt to remove some unnecessary menu options for Vendors/Members
     *
     * @access public
     * @since  2.0
     * @return void
     */
    public function cleanup_admin_menu() {
        if ( ! current_user_can( 'delete_plugins' ) && ! current_user_can( 'delete_themes' ) ) {
            remove_menu_page( 'wppusher' );
            remove_menu_page( 'profile.php' );
        }
    }
}