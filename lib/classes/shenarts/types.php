<?php

namespace ShenArts;

/**
 * Class Types
 * Implements any custom post types used within this theme
 * @package ShenArts
 */
class Types {
	/**
	 * List of the post type handles that need to be registered
	 *
	 * @since  2.0
	 * @access private
	 * @var array
	 */
	private $handles;
	/**
	 * Holds the class instance.
	 *
	 * @since     2.0
	 * @access    private
	 * @var       \ShenArts\Types
	 */
	private static $instance;

	/**
	 * Returns the instance of this class.
	 *
	 * @access  public
	 * @since   2.0
	 * @return  \ShenArts\Types
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className      = __CLASS__;
			self::$instance = new $className;
		}

		return self::$instance;
	}

	/**
	 * Create the object and set up the appropriate actions
	 *
	 * @access  private
	 * @since   2.0
	 */
	private function __construct() {
		$this->_set_handles( array( 'sponsor' ) );
		$this->register_post_types();
	}

	/**
	 * Fills the handles var
	 *
	 * @access private
	 * @since  2.0
	 * @return void
	 */
	private function _set_handles( $handles ) {
		$this->handles = $handles;
	}

	/**
	 * Retrieve the value of the handles var
	 *
	 * @access private
	 * @since  2.0
	 * @return array the value of the handles var
	 */
	private function _get_handles() {
		return $this->handles;
	}

	/**
	 * Register any necessary post types
	 *
	 * @access public
	 * @since  2.0
	 * @return void
	 */
	public function register_post_types() {
		foreach ( $this->_get_handles() as $handle ) {
			register_post_type( $handle, $this->get_args( $handle ) );
		}
	}

	public function get_args( $handle ) {
		if ( method_exists( $this, '_get_args_' . $handle ) ) {
			return call_user_func( array( $this, '_get_args_' . $handle ) );
		}
	}

	public function get_labels( $handle ) {
		if ( method_exists( $this, '_get_labels_' . $handle ) ) {
			return call_user_func( array( $this, '_get_labels_' . $handle ) );
		}
	}

	private function _get_args_sponsor() {
		$handle = 'sponsor';

		return array(
			"label"                 => __( "Sponsors", "" ),
			"labels"                => $this->get_labels( $handle ),
			"description"           => "",
			"public"                => true,
			"publicly_queryable"    => true,
			"show_ui"               => true,
			"delete_with_user"      => false,
			"show_in_rest"          => true,
			"rest_base"             => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive"           => false,
			"show_in_menu"          => true,
			"show_in_nav_menus"     => false,
			"exclude_from_search"   => true,
			"capability_type"       => "page",
			"map_meta_cap"          => true,
			"hierarchical"          => false,
			"rewrite"               => array( "slug" => "sponsor", "with_front" => false ),
			"query_var"             => true,
			"menu_icon"             => "dashicons-heart",
			"supports"              => array( "title", "thumbnail", "editor" ),
		);
	}

	private function _get_labels_sponsor() {
		$handle = 'sponsor';

		return array(
			"name"          => __( "Sponsors", "" ),
			"singular_name" => __( "Sponsor", "" ),
		);
	}
}