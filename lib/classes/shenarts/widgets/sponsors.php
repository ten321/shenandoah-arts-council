<?php

namespace ShenArts\Widgets;

use ShenArts\theme;

class Sponsors extends \WP_Widget {
    private $version = null;
    private $script_vars = array();

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
	    $this->version = theme::instance()->version;

		$widget_ops = array(
			'classname'   => 'shenarts_sponsors',
			'description' => 'Implements the display of sponsor information in a widget',
		);
		parent::__construct( 'shenarts_sponsors', __( 'Sponsor', 'shenarts' ), $widget_ops );

		$script_location = sprintf( get_stylesheet_directory_uri() . '/lib/scripts/shenarts/widgets/sponsors.%s', theme::instance()->script_extension );
		wp_register_script( 'shenarts-sponsor-widget', $script_location, array( 'jquery' ), $this->version, true );

		$this->script_vars = array(
		    'url' => get_rest_url( get_current_blog_id(), '/wp/v2/sponsor/' ),
            'selector' => '.sponsors-widget-content',
            'id_base' => '#sponsors-widget-content-',
        );
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		$htmlid = sprintf( 'sponsors-widget-content-%s', $this->id );
		echo '<ul class="sponsors-widget-content" id="' . $htmlid . '">';
        echo '<li class="waiting"><div></div><div></div><div></div><div></div></li>';
		echo '</ul>';
		echo $args['after_widget'];

		$defaults = array(
		    'title' => '',
            'number' => 5,
            'orderby' => 'date',
            'order' => 'desc',
        );

		$instance = array_merge( $defaults, $instance );

        $this->script_vars['instance'][$htmlid] = array(
            'widget_id' => $htmlid,
            'args' => $instance,
        );

        wp_enqueue_script( 'shenarts-sponsor-widget' );
        add_action( 'wp_print_footer_scripts', array( $this, 'print_scripts' ) );
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$number = ! empty( $instance['number'] ) ? intval( $instance['number'] ) : 5;
		$orderby = ! empty( $instance['orderby'] ) ? $instance['orderby'] : 'date';
		$order = $instance['order'] == 'asc' ? 'asc' : 'desc';

		$order_opts = array(
		    'date' => __( 'Post Date', 'shenarts' ),
            'title' => __( 'Post Title', 'shenarts' ),
            'menu_order' => __( 'Menu Order', 'shenarts' ),
            'rand' => __( 'Random', 'shenarts' ),
        );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_attr_e( 'How many sponsors should be shown?', 'shenarts' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="number" value="<?php echo intval( $number ); ?>">
		</p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_attr_e( 'Sort the results by:', 'shenarts' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'orderby' ) ); ?>">
                <?php foreach( $order_opts as $k => $v ) { ?>
                    <option value="<?php echo $k ?>"<?php selected( $orderby, $k, true ) ?>><?php echo $v ?></option>
            <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('order' ) ); ?>"><?php esc_attr_e( 'In which order?', 'shenarts' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>">
                <option value="asc"<?php selected( $order, 'asc', true ); ?>><?php _e( 'Ascending Order', 'shenarts' ); ?></option>
                <option value="desc"<?php selected( $order, 'desc', true ); ?>><?php _e( 'Descending Order', 'shenarts' ); ?></option>
            </select>
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
		$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? intval( $new_instance['number'] ) : 5;
		$instance['orderby'] = ( ! empty( $new_instance['orderby'] ) ) ? $new_instance['orderby'] : 'date';
		$instance['order'] = ( ! empty( $new_instance['order'] ) ) ? $new_instance['order'] : 'desc';

		return $instance;
	}

	public function print_scripts() {
	    printf( '<script type="text/javascript">let SASponsorsObj = %s;</script>', json_encode( $this->script_vars ) );
    }
}